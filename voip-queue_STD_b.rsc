#########################################################################################################
# Features                                                                                              #                                                                             #
# -Provides 256K of bandwidth for each VOIP line.                                                       #
#########################################################################################################

############################################################################################################################
#### Step 1 - Copy the queue onto the router.                                                                             ##
#### Step 2 - Identify the VOIP traffic. In this example, 3 phones have bee identified by their IP address.               ##
#### Step 3 - Adjust the Parent Queue upload and download speed to max speed delivered to the router.                     ##
#### Step 4 - Adjust the VOIP queue and the "All Other Traffic" Queue so that their max upload and download is equal to or##
####          less than the Parent Queue.                                                                                 ##
#### Step 5 - Adjust the limit-at speed on the VOIP queue so that the speed is set to 250K X #_of_VOIP_Streams. In this   ##
####          example, there was three lines, so limit-at was set to 750K.                                                ##
############################################################################################################################


/queue simple

add max-limit=1M/25M name="Office Parent Queue" target=10.254.254.0/24
add limit-at=750k/750k max-limit=1M/2M name="VOIP Traffic" parent="Office Parent Queue" target=10.254.254.231/32,10.254.254.232/32,10.254.254.233/32
add max-limit=1M/25M name="All Other Traffic" parent="Office Parent Queue" queue=pcq-upload-default/pcq-download-default target=10.254.254.0/24

/
