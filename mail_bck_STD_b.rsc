/ system script
add name="backup_mail" source="/system backup save name=email_backup \n/tool \
   e-mail send file=email_backup.backup to=\"someone@somewhere.tld\" body=\"See \
   attached file for System Backup\" subject=\(\[/system identity get name\] \
   . \" \" .  \[/system clock get time\] . \" \" . \[/system clock get date\] \
   . \"  Backup\"\)\n"

/ system scheduler
add name="sched_backup_mail" on-event="backup_mail" start-date=jan/01/1970 start-time=07:30:00 interval=7d \
comment="" disabled=no
