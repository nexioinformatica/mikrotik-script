# Script which will download the drop list as a text file
/system script add name="DownloadSpamhaus" source={
/tool fetch url="http://joshaven.com/spamhaus.rsc" mode=http;
:log info "Downloaded spamhaus.rsc from Joshaven.com";
}

# Script which will Remove old Spamhaus list and add new one
/system script add name="ReplaceSpamhaus" source={
/ip firewall address-list remove [find where comment="SpamHaus"]
/import file-name=spamhaus.rsc;
:log info "Removed old Spamhaus records and imported new list";
}

# Schedule the download and application of the spamhaus list
/system scheduler add comment="Download spamnaus list" interval=3d \
  name="DownloadSpamhausList" on-event=DownloadSpamhaus \
  start-date=jan/01/1970 start-time=22:06:53
/system scheduler add comment="Apply spamnaus List" interval=3d \
  name="InstallSpamhausList" on-event=ReplaceSpamhaus \
  start-date=jan/01/1970 start-time=22:11:53

#######

# Script which will download the drop list as a text file
/system script add name="Download_dshield" source={
/tool fetch url="http://joshaven.com/dshield.rsc" mode=http;
:log info "Downloaded dshield.rsc from Joshaven.com";
}

# Script which will Remove old dshield list and add new one
/system script add name="Replace_dshield" source={
/ip firewall address-list remove [find where comment="DShield"]
/import file-name=dshield.rsc;
:log info "Removed old dshield records and imported new list";
}

# Schedule the download and application of the dshield list
/system scheduler add comment="Download dshield list" interval=3d \
  name="DownloadDShieldList" on-event=Download_dshield \
  start-date=jan/01/1970 start-time=22:16:53
/system scheduler add comment="Apply dshield List" interval=3d \
  name="InstallDShieldList" on-event=Replace_dshield \
  start-date=jan/01/1970 start-time=22:21:53

#######

# Script which will download the malc0de list as a text file
/system script add name="Download_malc0de" source={
/tool fetch url="http://joshaven.com/malc0de.rsc" mode=http;
:log info "Downloaded malc0de.rsc from Joshaven.com";
}

# Script which will Remove old malc0de list and add new one
/system script add name="Replace_malc0de" source={
/ip firewall address-list remove [find where comment="malc0de"]
/import file-name=malc0de.rsc;
:log info "Removed old malc0de records and imported new list";
}

# Schedule the download and application of the malc0de list
/system scheduler add comment="Download malc0de list" interval=3d \
  name="Downloadmalc0deList" on-event=Download_malc0de \
  start-date=jan/01/1970 start-time=22:16:53
/system scheduler add comment="Apply malc0de List" interval=3d \
  name="Installmalc0deList" on-event=Replace_malc0de \
  start-date=jan/01/1970 start-time=22:21:53

########

# Script which will download the voip-bl list as a text file
/system script add name="Download_voip-bl" source={
/tool fetch url="http://joshaven.com/voip-bl.rsc" mode=http;
:log info "Downloaded voip-bl.rsc from Joshaven.com";
}

# Script which will Remove old voip-bl list and add new one
/system script add name="Replace_voip-bl" source={
/ip firewall address-list remove [find where comment="VoIP BL"]
/import file-name=voip-bl.rsc;
:log info "Removed old voip-bl records and imported new list";
}

# Schedule the download and application of the voip-bl list
/system scheduler add comment="Download voip-bl list" interval=3d \
  name="Refresh_voip-bl" on-event=Download_voip-bl \
  start-date=jan/01/1970 start-time=22:16:53
/system scheduler add comment="Apply voip-bl List" interval=3d \
  name="Update_voip-bl" on-event=Replace_voip-bl \
  start-date=jan/01/1970 start-time=22:21:53
